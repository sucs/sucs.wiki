# Once upon a time
Sup, SUCS provides email, while we have some pages on how to use it we have 0 info on htf it's setup, so this is how *I* think it works by trying to read and failing to understand config files.

This is page is more for me than anyone else, it's intended audience is a linux sysadmin that understands email in a linux shop.


# u w0t m80
SUCS has multiple domains (also no docs for this!!!!) but we seem to only care for email for the following:

* sucs.org
* sucs.swan.ac.uk 
* lists.sucs.org
* lists.sucs.swan.ac.uk


# 2hard(core)4u
There's 2 servers, silver and outpost. I'll talk about outpost first because that's quicker.

## outpost
A VM (with ipv6 connectivity) provided by firefury, hosts: mw (secondary), dns slave, and *backup mx*!. It's admin'd by us, but not all of us, I have a key to it, firefury does, dunno who else.

It's debian jessie (as of writing). Pretty much default debian install of exim setup as a smarthost to forward sucs.org and sucs.swan.ac.uk mail to silver. It has greylistd/whatever it's called installed. Does no spf or spam or anything like that, super simple.

# silver
Oh boy....

## smtp
debian. exim. internet site.

```
dc_eximconfig_configtype='internet'
dc_other_hostnames='sucs.org; sucs.swan.ac.uk; silver.sucs.org; silver.sucs.swan.ac.uk; society.swan.ac.uk; lists.sucs.org; lists.sucs.swan.ac.uk'
dc_relay_nets='137.44.10.0/25; 137.44.6.5'
dc_use_split_config='true'
dc_localdelivery='mail_spool'
```

### in
smtp to sucs.org

exim is TLS'd using letsencrypt for (www.)sucs.org *ONLY* (we should take advantage of `${tls_sni}`)


Greylisting provided by, greylistd. 


```
 if *auth'd* (sasl->pam->ldap client->ldap server) sucs user:
     we trust it no matter what
 else
     if ip is shitlisted (`CHECK_RCPT_IP_DNSBLS` in `zen.dnsbl.ja.net:b.barracudacentral.org`)
        we reject it
     else
        trust
```

_some other stuff_

look at /etc/aliases to see if it's an alias also set `SYSTEM_ALIASES_PIPE_TRANSPORT` to 'lurker_pipe' for lurker 

if said user has a .forward file in their homedir, then use that

otherwise use /etc/procmail to deliver

/etc/procmail throws it at spamassassin, unless people have a ~/.nospamc (`* ! ? test -f $HOME/.nospamc`)

then to a users .procmailrc

then send it's to mbox in /var/mail by procmail (unless it's been sent to elsewhere in a user .procmail)


### out
if you're an auth sucs user you can send whatever as whoever :innocent: 


## imap&pop3
dovecot - doing imap and pop3 for sucs.org (ssl/tls). TLS'd using letsencrypt for (www.)sucs.org *ONLY*

auth against pam, plain over tls/ssl

```mail_location = mbox:~/mail:INBOX=/var/mail/%u```

## lurker
copy of exec@ and admin@ get saved to sucs.org/lurker

The exim router is just the system_alises one but with the `SYSTEM_ALIASES_PIPE_TRANSPORT` to 'lurker_pipe'


the emails get to it via the aliases in the /etc/aliases `"|/usr/bin/lurker-index -l exec -m"`

exim transport is as follows:
```
lurker_pipe:
  debug_print = "T: address_pipe for $local_part@$domain"
  group = lurker
  driver = pipe
  return_fail_output
```

## mailman
last but not least is mailman - https://lists.sucs.org

We have: lists.sucs.(org|swan.ac.uk) as the address, fairly little exim config.


vars:
```
# Home dir for your Mailman installation -- aka Mailman's prefix
# directory.
MAILMAN_HOME=/var/lib/mailman
MAILMAN_WRAP=MAILMAN_HOME/mail/mailman

# User and group for Mailman, should match your --with-mail-gid
# switch to Mailman's configure script.
MAILMAN_USER=list
MAILMAN_GROUP=daemon
```

router:
```
# Messages get sent out with
# envelope from "mailman-bounces@virtual_domain"
# But mailman doesn't put such addresses
# in the aliases. Recognise these here.
mailman_workaround:
  domains = lists.sucs.org : lists.sucs.swan.ac.uk 
  require_files = MAILMAN_HOME/lists/$local_part/config.pck
  driver = accept
  local_parts = mailman
  local_part_suffix_optional
  local_part_suffix = -bounces : -bounces+* : \
           -confirm+* : -join : -leave : \
           -subscribe : -unsubscribe : \
           -owner : -request : -admin : -loop
  transport = mailman_transport
  group = MAILMAN_GROUP

# Mailman lists
mailman_router:
  domains = +local_domains
  condition = ${lookup{$local_part@$domain}lsearch{MAILMAN_HOME/data/virtual-mailman}{1}{0}}
  require_files = MAILMAN_HOME/lists/$local_part/config.pck
  driver = accept
  local_part_suffix_optional
  local_part_suffix = -bounces : -bounces+* : \
                      -confirm+* : -join : -leave : \
                      -subscribe : -unsubscribe : \
                      -owner : -request : -admin : -loop
  transport = mailman_transport
  group = MAILMAN_GROUP
```
and transport:
```
mailman_transport:
  driver = pipe
  command = MAILMAN_WRAP \
            '${if def:local_part_suffix \
                  {${sg{$local_part_suffix}{-(\\w+)(\\+.*)?}{\$1}}} \
                  {post}}' \
            $local_part
  current_directory = MAILMAN_HOME
  home_directory = MAILMAN_HOME
  user = MAILMAN_USER
  group = MAILMAN_GROUP
  freeze_exec_fail = true
```