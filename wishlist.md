# Admin Wishlist

If we had money this is what we would want.


## Backup
Current backup is in a 3u or 4u case, that has no hotswap bays, only 6 3.5" internal hard drive bays.

New backup would ideally have front of rack facing hotswap drive bays. Needs to have at least 5.

Something like http://www.ebay.co.uk/itm/222386767821 would be cool. Would need to buy new 2.5" drives and caddys though.


## SUCS Central Storage
Move /home off of silver and VM storage off of iridium into a single NAS type solution.

It would give us a single point of failure but would mean growing storage would be easier and so would backups as you could just point the backup server at one endpoint.

We currently don't backup VMs at all, don't have the space or a system in place.


## SUCS Switch
A new switch that can do the following would be nice:
1. 802.1ab
2. 802.1Q + 802.3ac
3. 802.1ad
4. 802.1x

## Colour laser printer
Colour > Black and White.