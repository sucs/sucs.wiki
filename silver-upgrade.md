## The goal is to get to jessie.

Than plan is to clean install + config via ansible

# Critical

- [ ] Network (137.44.10.1 & 137.44.10.61)
- [ ] BIND - DNSSEC?
- [x] LDAP (*dump old DATA ONLY, re-setup empty db with jessie default: mdb, ppolicy etc... then import user data*)
- [ ] LDAP Client - so people can log in :)
- [ ] /home (zfs???)
- [ ] exim4 - convert from mbox to maildir? spf+dkim+dmarc? 
- [ ] SSH (put host keys back)
- [ ] cam - motion config file syntax chnage...
- [ ] add ssh key for backup so it can login and backup
- [x] UPS - shut down on power outage


# Optional

- [x] Milliways (there is a jessie branch)
- [ ] Postgres (config doesn't need changing, dump and restore dbs)
- [ ] mysql
- [ ] apache2
- [ ] Mount backup and configure idmap
- [ ] Squid
