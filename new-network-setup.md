# Proposed Network Changes

If we got a switch that did 802.1Q and 802.3ac then I suggest we should lay out our /24 like this:

* 137.44.10.0/28 - SUCS Servers/Services
* 137.44.10.16/28 - Insecure Devices
* 137.44.10.32/27 - Desktops
* 137.44.10.64/26 - Public VMNet
* 137.44.10.128/25 - GuestNet

Each subnet would be its own vlan